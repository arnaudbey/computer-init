# computer-init

## Paquets de base
  ```
  sudo apt-get update

  sudo apt-get install \
    terminator \
    zsh \
    make \
    vim \
    curl \
    docker.io \
    docker-compose \
    git \
    xclip
  ```


## Plugins atom
  ```
  apm install \
    atom-beautify \
    language-dotenv \
    php-twig \
    sort-lines
  ```

## OhMyZsh
> voir https://ohmyz.sh/

`sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`

## SSH
> voir https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair

```
ssh-keygen -t ed25519 -C "email@example.com"
xclip -sel clip < ~/.ssh/id_ed25519.pub
```

## Docker & DNS (derrière le réseau de la fac par exemple)
> voir https://stackoverflow.com/questions/24991136/docker-build-could-not-resolve-archive-ubuntu-com-apt-get-fails-to-install-a

```
nmcli dev show | grep 'IP4.DNS'
vi /etc/docker/daemon.json
```
> {"dns": ["10.0.0.2", "10.0.0.3"]}

```
sudo service docker restart
```
